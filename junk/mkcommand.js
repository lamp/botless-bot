var https = require("https");
var config = require('./config');

https.request(`https://discord.com/api/v8/applications/${config.application_id}/commands`, {
	method: "POST",
	headers: {
		"Authorization": `Bot ${config.bot_token}`,
		"Content-Type": "application/json"
	}
}).end(JSON.stringify({
	name: "foo",
	type: 1,
	description: "foo bar"
}));


https.request(`https://discord.com/api/v8/applications/${config.application_id}/commands`, {
	method: "POST",
	headers: {
		"Authorization": `Bot ${config.bot_token}`,
		"Content-Type": "application/json"
	}
}).end(JSON.stringify({
	name: "asdf",
	type: 1,
	description: "test"
}));