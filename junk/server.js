var http = require("http");
var crypto = require("crypto");
var config = require("./config");

var PUBLIC_KEY = Buffer.from(config.public_key, "hex");

var server = http.createServer(function (req, res) {
	if (req.method != "POST") return res.writeHead(405).end();
	var body = "";
	req.on("data", data => body += String(data));
	req.on("end", () => {
		crypto.verify(
			"ed25519",
			Buffer.from(req.headers["X-Signature-Timestamp"] + body),
			PUBLIC_KEY,
			Buffer.from(req.headers["X-Signature-Ed25519"], "hex"),
			function (error, result) {
				if (error || !result) {
					if (error) console.error(error.message);
					console.log("bad signature");
					res.writeHead(401).end();
					return;
				}
				try {
					var data = JSON.parse(body);
					switch (data.type) {
						case 1: // PING
							res.end(`{"type":1}`); // PONG
							break;
						case 2: // APPLICATION_COMMAND
							switch (data.data.name) {
								case "foo":
									res.writeHead(200, {
										"Content-Type": "application/json"
									});
									res.end(JSON.stringify({
										type: 4, // CHANNEL_MESSAGE_WITH_SOURCE
										data: {
											content: "bar"
										}
									}));
									break;
								default:
									res.writeHead(200, {
										"Content-Type": "application/json"
									});
									res.end(JSON.stringify({
										type: 4, // CHANNEL_MESSAGE_WITH_SOURCE
										data: {
											content: JSON.stringify(data.data, null, 4) // test
										}
									}));
									break;
							}
							break;
						case 3: // MESSAGE_COMPONENT
							res.writeHead(204).end();
							break;
						default: // unknown
							res.writeHead(204).end();
							break;
					}
				} catch(error) {
					console.error(error.message);
					res.writeHead(500).end();
				}
			}
		);

	});
});

server.listen(28459);





