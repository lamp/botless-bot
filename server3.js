var express = require("express");
var nacl = require("tweetnacl");
var config = require("./config");
var PUBLIC_KEY = Buffer.from(config.public_key, 'hex');
var app = express();
app.set("trust proxy", "127.0.0.1");

// need to parse body as text for verification
app.use(express.text({type: "application/json"}));

// verify signature
app.use(function (req, res, next) {
	var signature = req.headers["x-signature-ed25519"];
	var timestamp = req.headers["x-signature-timestamp"];
	if (!signature || !timestamp) return res.status(400).send("Missing headers");
	var verified = nacl.sign.detached.verify(
		Buffer.from(timestamp + req.body),
		Buffer.from(signature, 'hex'),
		PUBLIC_KEY
	);
	if (verified) next();
	else res.sendStatus(401);
});

// now we need it as json
app.use(function (req, res, next) {
	try {
		req.body = JSON.parse(req.body);
		next();
	} catch (error) {
		next(error);
	}
});

// debug
/*app.use(function (req, res, next) {
	console.debug("req.body", typeof req.body, req.body);
	var send = res.send;
	res.send = function() {
		console.log("res.send:", arguments);
		send.apply(res, arguments);
	};
	next();
});*/

// main part
app.post('/', function (req, res) {
	switch (req.body.type) {
		case 1: // PING
			res.send({type: 1}); // PONG
			break;
		case 2: // APPLICATION_COMMAND
			res.send({
				type: 4, // CHANNEL_MESSAGE_WITH_SOURCE
				data: {
					content: "blurb" // testing
				}
			})
			break;
		case 3: // MESSAGE_COMPONENT
		default: // unknown
			res.sendStatus(204); // app has nothing to say for this interaction
			break;
	}
});

app.listen(28459, "127.0.0.1");
